import { CssBaseline, ThemeProvider } from '@material-ui/core'
import Editor from 'pages/Editor'
import MagicItemInformation from 'pages/MagicItemInformation'
import ModuleSelection from 'pages/ModuleSelection'
import React from 'react'
import { Provider } from 'react-redux'
import { HashRouter, Redirect, Route, Switch } from 'react-router-dom'
import store from 'store/store'
import { ModifiedTheme } from 'utils/theme'

function App(): JSX.Element {
    return (
        <>
            <HashRouter>
                <Provider store={store}>
                    <ThemeProvider theme={ModifiedTheme}>
                        <CssBaseline />
                        <Switch>
                            <Route exact={true} path={'/Editor'} component={Editor} />
                            <Route exact={true} path={'/ModuleSelection'} component={ModuleSelection} />
                            <Route exact={true} path={'/MagicItemInformation'} component={MagicItemInformation} />
                            <Route>
                                <Redirect from={'/'} to={'/ModuleSelection'} />
                            </Route>
                        </Switch>
                    </ThemeProvider>
                </Provider>
            </HashRouter>
        </>
    )
}

export default App

import { Character } from 'utils/types'

export enum GeneralStateAction {
    DEFAULT = 'DEFAULT', // This is only for testing purposes
    SET_VALUE = 'SET_VALUE',
    SET_CHARACTERS = 'SET_CHARACTERS',
    ADD_CHARACTER = 'ADD_CHARACTER',
    REMOVE_CHARACTER = 'REMOVE_CHARACTER',
    SET_SELECTED_CHARACTER = 'SET_SELECTED_CHARACTER',
    SAVE_CHARACTER = 'SAVE_CHARACTER',
    IMPORT_CHARACTER = 'IMPORT_CHARACTER',
}

type GeneralStateActions =
    | { type: GeneralStateAction.DEFAULT }
    | { type: GeneralStateAction.SET_VALUE; payload: number }
    | { type: GeneralStateAction.ADD_CHARACTER; payload: string }
    | { type: GeneralStateAction.REMOVE_CHARACTER; payload: string }
    | { type: GeneralStateAction.SAVE_CHARACTER; payload: Character }
    | { type: GeneralStateAction.SET_CHARACTERS; payload: Character[] }
    | { type: GeneralStateAction.SET_SELECTED_CHARACTER; payload: string | undefined }
    | { type: GeneralStateAction.IMPORT_CHARACTER; payload: Character }

export default GeneralStateActions

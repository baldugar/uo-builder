import GeneralStateActions, { GeneralStateAction } from 'store/GeneralState/GeneralState.actions'
import testData from 'utils/mockData.json'
import { Character } from 'utils/types'
import { v4 as uuidv4 } from 'uuid'

export interface GeneralState {
    value: number
    characters: Character[]
    selectedCharacterID: string | undefined
}

export const initialGeneralState: GeneralState = {
    value: 0,
    characters: [],
    selectedCharacterID: undefined,
}

function GeneralStateReducer(state: GeneralState = initialGeneralState, action: GeneralStateActions): GeneralState {
    switch (action.type) {
        case GeneralStateAction.SET_VALUE:
            return {
                ...state,
                value: action.payload,
            }
        case GeneralStateAction.ADD_CHARACTER:
            const id = uuidv4()
            return {
                ...state,
                selectedCharacterID: state.selectedCharacterID === undefined ? id : state.selectedCharacterID,
                characters: [
                    ...state.characters,
                    {
                        id,
                        name: action.payload,
                        skillSet: {
                            skills: [],
                        },
                        stats: {
                            dexterity: 0,
                            intelligence: 0,
                            strength: 0,
                        },
                    },
                ],
            }
        case GeneralStateAction.REMOVE_CHARACTER:
            return {
                ...state,
                characters: [...state.characters.filter((char) => char.id !== action.payload)],
            }
        case GeneralStateAction.SET_CHARACTERS:
            return {
                ...state,
                characters: [...action.payload],
            }
        case GeneralStateAction.SET_SELECTED_CHARACTER:
            return {
                ...state,
                selectedCharacterID: action.payload,
            }
        case GeneralStateAction.SAVE_CHARACTER:
            return {
                ...state,
                characters: [...state.characters.map((c) => (c.id === action.payload.id ? action.payload : c))],
            }
        case GeneralStateAction.IMPORT_CHARACTER:
            return {
                ...state,
                characters: [...state.characters, action.payload],
            }
        default:
            return state
    }
}

export default GeneralStateReducer

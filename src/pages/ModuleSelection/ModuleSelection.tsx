import { Box, Card, CardActionArea, Container, Grid, Tooltip } from '@material-ui/core'
import React from 'react'
import { RouteComponentProps } from 'react-router-dom'
import Editor from '../../assets/icon1.png'
import Items from '../../assets/icon2.png'

const ModuleSelection = (props: RouteComponentProps): JSX.Element => {
    const { history } = props
    return (
        <Box width={'100vw'} maxWidth={'100vw'}>
            <Container
                maxWidth={'lg'}
                style={{ paddingTop: 50, display: 'flex', flexDirection: 'column', justifyContent: 'center' }}
            >
                <Grid container spacing={4} justify={'center'} alignItems={'flex-end'}>
                    <Grid item xs={6} md={4}>
                        <Tooltip title={'Character Editor'} placement={'top'}>
                            <Card style={{ width: '100%', paddingTop: '100%', position: 'relative' }}>
                                <CardActionArea
                                    style={{
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        bottom: 0,
                                        right: 0,
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    onClick={() => history.push('Editor')}
                                >
                                    <img src={Editor} width={'100%'} />
                                </CardActionArea>
                            </Card>
                        </Tooltip>
                    </Grid>
                    <Grid item xs={6} md={4}>
                        <Tooltip title={'Magical Item Information'} placement={'top'}>
                            <Card style={{ width: '100%', paddingTop: '100%', position: 'relative' }}>
                                <CardActionArea
                                    style={{
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        bottom: 0,
                                        right: 0,
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    onClick={() => history.push('MagicItemInformation')}
                                >
                                    <img src={Items} width={'100%'} />
                                </CardActionArea>
                            </Card>
                        </Tooltip>
                    </Grid>
                </Grid>
            </Container>
        </Box>
    )
}

export default ModuleSelection

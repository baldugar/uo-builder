import {
    Box,
    ButtonBase,
    Card,
    CardHeader,
    Container,
    Divider,
    Grid,
    Fade,
    TextField,
    Typography,
    CardContent,
} from '@material-ui/core'
import React, { useCallback, useState } from 'react'
import database from './database.json'

export interface Database {
    weapons: WeaponsOrInstrumentsOrSpellbooks
    armor: Armor
    instruments: WeaponsOrInstrumentsOrSpellbooks
    spellbooks: WeaponsOrInstrumentsOrSpellbooks
}
export interface WeaponsOrInstrumentsOrSpellbooks {
    names: string[]
    modifiers: ModifiersEntity[]
    materials: MaterialsEntity[]
}
export interface ModifiersEntity {
    name: string
    description: string
    steps: StepsEntityOrModifiersEntity[]
}
export interface StepsEntityOrModifiersEntity {
    name: string
    value: number
}
export interface MaterialsEntity {
    name: string
    modifiers: StepsEntityOrModifiersEntity[]
}
export interface Armor extends WeaponsOrInstrumentsOrSpellbooks {
    armorMaterials: ArmorMaterialsEntity[]
}
export interface ArmorMaterialsEntity {
    name: string
}
export enum Item {
    WEAPONS = 'weapons',
    ARMOR = 'armor',
    INSTRUMENTS = 'instruments',
    SPELLBOOKS = 'spellbooks',
    UNKNOWN = 'unknown',
}

export interface ShowValue {
    name: string
    value: number
}

export interface ShowCategory {
    name: string
    description: string
    values: ShowValue[]
}

const getItemCategory = (itemName: string, db: Database): Item => {
    const weaponNames = [...db.weapons.names]
    const armorNames = [...db.armor.names]
    const instrumentNames = [...db.instruments.names]
    const spellbookNames = [...db.spellbooks.names]
    weaponNames.sort((a, b) => b.split(' ').length - a.split(' ').length)
    armorNames.sort((a, b) => b.split(' ').length - a.split(' ').length)
    instrumentNames.sort((a, b) => b.split(' ').length - a.split(' ').length)
    spellbookNames.sort((a, b) => b.split(' ').length - a.split(' ').length)
    for (let i = 0; i < weaponNames.length; i++) {
        if (itemName.toUpperCase().includes(weaponNames[i].toUpperCase())) {
            return Item.WEAPONS
        }
    }
    for (let i = 0; i < armorNames.length; i++) {
        if (itemName.toUpperCase().includes(armorNames[i].toUpperCase())) {
            return Item.ARMOR
        }
    }
    for (let i = 0; i < instrumentNames.length; i++) {
        if (itemName.toUpperCase().includes(instrumentNames[i].toUpperCase())) {
            return Item.INSTRUMENTS
        }
    }
    for (let i = 0; i < spellbookNames.length; i++) {
        if (itemName.toUpperCase().includes(spellbookNames[i].toUpperCase())) {
            return Item.SPELLBOOKS
        }
    }
    return Item.UNKNOWN
}

const getItemModifiers = (
    itemName: string,
    itemType: Item,
    db: Database,
    currentShowCategories: ShowCategory[],
): ShowCategory[] => {
    console.log(itemName, itemType)
    const modifiers =
        itemType === Item.WEAPONS
            ? db.weapons.modifiers
            : itemType === Item.ARMOR
            ? db.armor.modifiers
            : itemType === Item.INSTRUMENTS
            ? db.instruments.modifiers
            : itemType === Item.SPELLBOOKS
            ? db.spellbooks.modifiers
            : []
    for (let i = 0; i < modifiers.length; i++) {
        const mod = modifiers[i]
        mod.steps.sort((a, b) => b.name.split(' ').length - a.name.split(' ').length)
    }
    console.log('CURRENT MODS', modifiers)
    const categories: ShowCategory[] = [...currentShowCategories]
    modifiers.forEach((mod) => {
        mod.steps.forEach((step) => {
            if (itemName.toUpperCase().includes(step.name.toUpperCase())) {
                const categoryIndex = categories.findIndex((cat) => cat.name.toUpperCase() === mod.name.toUpperCase())
                if (categoryIndex !== -1) {
                    categories[categoryIndex].values.push({
                        name: step.name,
                        value: step.value,
                    })
                } else if (step.value > 0) {
                    categories.push({
                        description: mod.description,
                        name: mod.name,
                        values: [
                            {
                                name: step.name,
                                value: step.value,
                            },
                        ],
                    })
                }
            }
        })
    })
    return categories
}

const getItemMaterial = (
    itemName: string,
    itemType: Item,
    db: Database,
    currentShowCategories: ShowCategory[],
): ShowCategory[] => {
    const materials =
        itemType === Item.WEAPONS
            ? [...db.weapons.materials]
            : itemType === Item.ARMOR
            ? [...db.armor.materials]
            : itemType === Item.INSTRUMENTS
            ? [...db.instruments.materials]
            : itemType === Item.SPELLBOOKS
            ? [...db.spellbooks.materials]
            : []
    const categories: ShowCategory[] = [...currentShowCategories]
    materials.sort((a, b) => b.name.split(' ').length - a.name.split(' ').length)
    const mat = materials.find((mat) => mat.name !== '' && itemName.toUpperCase().includes(mat.name.toUpperCase()))
    if (mat) {
        mat.modifiers.forEach((mod) => {
            if (mod.value > 0) {
                const categoryIndex = categories.findIndex((cat) => cat.name.toUpperCase() === mod.name.toUpperCase())
                if (categoryIndex !== -1) {
                    categories[categoryIndex].values.push({
                        name: mat.name,
                        value: mod.value,
                    })
                } else {
                    const modifiers =
                        itemType === Item.WEAPONS
                            ? db.weapons.modifiers
                            : itemType === Item.ARMOR
                            ? db.armor.modifiers
                            : itemType === Item.INSTRUMENTS
                            ? db.instruments.modifiers
                            : itemType === Item.SPELLBOOKS
                            ? db.spellbooks.modifiers
                            : []
                    const modifier = modifiers.find(
                        (modifier) => modifier.name.toUpperCase() === mod.name.toUpperCase(),
                    )
                    if (modifier) {
                        categories.push({
                            description: modifier.description,
                            name: modifier.name,
                            values: [
                                {
                                    name: mat.name,
                                    value: mod.value,
                                },
                            ],
                        })
                    }
                }
            }
        })
    }
    return categories
}

const MagicItemInformation = (): JSX.Element => {
    const [input, setInput] = useState('')
    const [results, setResults] = useState<ShowCategory[]>()
    const [error, setError] = useState<string>()

    const db = database as Database

    return (
        <>
            <Box height={'100vh'} maxHeight={'100vh'} width={'100vw'} maxWidth={'100vw'}>
                <Box>
                    <Typography id={'header'} variant={'h2'} className={'ExampleText'} align={'center'}>
                        Magic Item Information: Outlands
                    </Typography>
                </Box>
                <Box>
                    <Typography id={'header'} variant={'h4'} className={'ExampleText'} align={'center'}>
                        Write your magic item&apos;s name and check out what it is!
                    </Typography>
                </Box>
                <Box
                    paddingTop={2}
                    display={'flex'}
                    flexDirection={'row'}
                    style={{
                        height: `calc(100% - 72px`,
                        maxHeight: `calc(100% - 72px`,
                    }}
                >
                    <Container maxWidth={'md'}>
                        <Box width={1} display={'flex'} mb={4}>
                            <TextField
                                variant={'outlined'}
                                fullWidth
                                value={input}
                                onChange={(e) => {
                                    setError(undefined)
                                    setInput(e.target.value)
                                }}
                            />

                            <ButtonBase
                                onClick={() => {
                                    setError(undefined)
                                    const itemCategory = getItemCategory(input, db)
                                    if (itemCategory === Item.UNKNOWN) {
                                        setError('Item not recognized')
                                        setResults(undefined)
                                    } else {
                                        const sc = getItemMaterial(
                                            input,
                                            itemCategory,
                                            db,
                                            getItemModifiers(input, itemCategory, db, []),
                                        )
                                        setResults(sc)
                                    }
                                }}
                                style={{
                                    marginLeft: 32,
                                    paddingLeft: 16,
                                    paddingRight: 16,
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    flex: 1,
                                    borderRadius: 8,
                                    backgroundColor: 'rgba(0,0,0,0.12)',
                                    boxShadow: '0px 1px 4px 0px rgba(0,0,0,0.75)',
                                }}
                            >
                                <Typography variant={'button'}>Analize</Typography>
                            </ButtonBase>
                        </Box>
                        <Fade in={results !== undefined}>
                            <Box display={'flex'}>
                                {results !== undefined && (
                                    <Grid container spacing={3}>
                                        {results.map((result, index) => (
                                            <Grid key={result.name + index} item xs={12} sm={4}>
                                                <Card
                                                    style={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                                                >
                                                    <CardHeader title={result.name} subheader={result.description} />
                                                    <CardContent
                                                        style={{
                                                            display: 'flex',
                                                            flexDirection: 'column',
                                                            justifyContent: 'flex-end',
                                                            flex: 1,
                                                        }}
                                                    >
                                                        {result.values.map((val, index) => (
                                                            <Box
                                                                key={val.name + index}
                                                                width={1}
                                                                display={'flex'}
                                                                justifyContent={'space-between'}
                                                                mb={1}
                                                            >
                                                                <Typography>{val.name}</Typography>
                                                                <Typography>{val.value}</Typography>
                                                            </Box>
                                                        ))}
                                                        <Divider />
                                                        <Box
                                                            mt={1}
                                                            width={1}
                                                            display={'flex'}
                                                            justifyContent={'space-between'}
                                                        >
                                                            <Typography>Total</Typography>
                                                            <Typography>
                                                                {result.values.reduce(
                                                                    (accItem, item) => accItem + item.value,
                                                                    0,
                                                                )}
                                                            </Typography>
                                                        </Box>
                                                    </CardContent>
                                                </Card>
                                            </Grid>
                                        ))}
                                    </Grid>
                                )}
                            </Box>
                        </Fade>
                        <Fade in={error !== undefined}>
                            <Box display={'flex'}>
                                {error !== undefined && <Typography variant={'h5'}>{error}</Typography>}
                            </Box>
                        </Fade>
                    </Container>
                </Box>
            </Box>
        </>
    )
}

export default MagicItemInformation

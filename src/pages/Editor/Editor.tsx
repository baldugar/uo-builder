import { Box, Button, Collapse, Fade, Modal, TextField } from '@material-ui/core'
import CircularProgress from '@material-ui/core/CircularProgress'
import { useTheme } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import CharacterEditor from 'components/CharacterEditor'
import CharacterList from 'components/CharacterList'
import { Base64 } from 'js-base64'
import * as React from 'react'
import { Character } from 'utils/types'
import { v4 as uuidv4 } from 'uuid'
import { useEditorState } from './useEditorState'

const EditorPage = (): JSX.Element => {
    const theme = useTheme()
    const {
        characters,
        selectedCharacterID,
        addCharacter,
        removeCharacter,
        setSelectedCharacter,
        setSkillURL,
        skillUrl,
        isLoading,
        saveCharacter,
        saveModalVisible,
        setSaveModalVisible,
        setCharacters,
        importCharacter,
    } = useEditorState()

    const selectedCharacter = React.useMemo(() => characters.find((c) => c.id === selectedCharacterID), [
        selectedCharacterID,
    ])
    const [clickModal, setClickModal] = React.useState(false)
    const [importDatabaseModalVisible, setImportDatabaseModalVisible] = React.useState(false)
    const [importCharacterModalVisible, setImportCharacterModalVisible] = React.useState(false)
    const [exportCharacterModalVisible, setExportCharacterModalVisible] = React.useState<Character>()
    const [nameIsEqual, setNameIsEqual] = React.useState(false)
    const [newName, setNewName] = React.useState('')

    if (isLoading) {
        return (
            <Box height={'100vh'} width={'100vw'} display={'flex'} justifyContent={'center'} alignItems={'center'}>
                <CircularProgress size={64} />
            </Box>
        )
    }

    return (
        <>
            <Box height={'100vh'} maxHeight={'100vh'} width={'100vw'} maxWidth={'100vw'}>
                <Box>
                    <Typography id={'header'} variant={'h2'} className={'ExampleText'} align={'center'}>
                        UO Character Editor: Outlands
                    </Typography>
                </Box>
                <Box
                    paddingTop={2}
                    paddingLeft={2}
                    paddingRight={2}
                    display={'flex'}
                    flexDirection={'row'}
                    style={{
                        height: `calc(100% - 72px`,
                        maxHeight: `calc(100% - 72px`,
                    }}
                >
                    <Box width={{ xs: 1, sm: '25%' }} height={1}>
                        <CharacterList
                            characters={characters}
                            selectedCharacterID={selectedCharacterID}
                            addCharacter={addCharacter}
                            removeCharacter={removeCharacter}
                            setSelectedCharacter={setSelectedCharacter}
                            setSaveModalVisible={setSaveModalVisible}
                            setImportDatabaseModal={setImportDatabaseModalVisible}
                            setImportCharacterModal={setImportCharacterModalVisible}
                            setExportCharacterModal={setExportCharacterModalVisible}
                        />
                    </Box>
                    <Box width={{ xs: 1, sm: '75%' }} height={1}>
                        <Fade in={characters.findIndex((c) => c.id === selectedCharacterID) !== -1}>
                            <Box height={1} display={'flex'}>
                                {selectedCharacter !== undefined && (
                                    <CharacterEditor
                                        character={selectedCharacter}
                                        saveCharacter={(char) => {
                                            saveCharacter(char)
                                        }}
                                        setSkillURL={(url) => setSkillURL(url)}
                                        skillUrl={skillUrl}
                                    />
                                )}
                            </Box>
                        </Fade>
                    </Box>
                </Box>
            </Box>
            <Modal
                open={
                    saveModalVisible ||
                    importDatabaseModalVisible ||
                    importCharacterModalVisible ||
                    exportCharacterModalVisible !== undefined
                }
                onClose={() => {
                    setSaveModalVisible(false)
                    setImportDatabaseModalVisible(false)
                    setImportCharacterModalVisible(false)
                    setExportCharacterModalVisible(undefined)
                }}
            >
                <Box
                    flex={1}
                    bgcolor={'rgba(0,0,0,0.12)'}
                    height={1}
                    display={'flex'}
                    justifyContent={'center'}
                    alignItems={'center'}
                    id={'modalBackdrop'}
                    onMouseDown={() => {
                        setClickModal(true)
                    }}
                    onMouseUp={() => {
                        if (clickModal) {
                            setSaveModalVisible(false)
                            setImportDatabaseModalVisible(false)
                            setImportCharacterModalVisible(false)
                            setExportCharacterModalVisible(undefined)
                        }
                        setClickModal(false)
                    }}
                >
                    <Box
                        maxHeight={'90vh'}
                        style={{
                            overflowY: 'auto',
                        }}
                        width={'60vw'}
                        maxWidth={'600px'}
                        display={'flex'}
                        flexDirection={'column'}
                        bgcolor={'#FAFAFA'}
                        padding={2}
                        borderRadius={16}
                        onMouseDown={(e) => {
                            e.stopPropagation()
                        }}
                        onMouseUp={(e) => {
                            e.stopPropagation()
                            setClickModal(false)
                        }}
                    >
                        {saveModalVisible && (
                            <>
                                <Typography variant={'h3'}>Characters are saved!</Typography>
                                <Typography>
                                    Your database has been saved on this browser. If you want to load it in another
                                    browser you can copy this code, you will be able to import your characters
                                    <Typography style={{ color: theme.palette.error.main }}>
                                        Make sure you have saved your characters before!
                                    </Typography>
                                </Typography>
                                <TextField
                                    variant={'outlined'}
                                    value={Base64.encode(JSON.stringify(characters))}
                                    multiline
                                    fullWidth
                                    style={{ marginTop: 8 }}
                                />
                            </>
                        )}
                        {exportCharacterModalVisible && (
                            <>
                                <Typography variant={'h3'}>Exporting character</Typography>
                                <Typography>
                                    If you copy this code, you will be able to import your character
                                    <Typography style={{ color: theme.palette.error.main }}>
                                        Make sure you have saved your character before!
                                    </Typography>
                                </Typography>
                                <TextField
                                    variant={'outlined'}
                                    value={Base64.encode(JSON.stringify(exportCharacterModalVisible))}
                                    multiline
                                    fullWidth
                                    style={{ marginTop: 8 }}
                                />
                            </>
                        )}
                        {importDatabaseModalVisible && (
                            <>
                                <Typography variant={'h3'}>Import an existing database</Typography>
                                <Typography>
                                    If you have a database code, paste it here and you&apos;ll import it.
                                    <Typography style={{ color: theme.palette.error.main }}>
                                        You will lose your current data!
                                    </Typography>
                                </Typography>
                                <TextField
                                    variant={'outlined'}
                                    multiline
                                    fullWidth
                                    inputProps={{ id: 'importField' }}
                                    style={{ marginTop: 16 }}
                                    rows={Math.floor(document.body.scrollHeight / 100)}
                                />
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={(e) => {
                                        e.stopPropagation()
                                        const input = document.getElementById('importField')
                                        if (input) {
                                            console.log(
                                                JSON.parse(Base64.decode((input as HTMLInputElement).value)),
                                                Base64.decode((input as HTMLInputElement).value),
                                                Base64.decode((input as HTMLInputElement).value).length,
                                                (input as HTMLInputElement).value,
                                                (input as HTMLInputElement).value.length,
                                            )
                                        }
                                        // setCharacters()
                                    }}
                                    style={{ marginTop: 16 }}
                                >
                                    Import
                                </Button>
                            </>
                        )}
                        {importCharacterModalVisible && (
                            <>
                                <Typography variant={'h3'}>Import an existing character</Typography>
                                <Typography>
                                    If you have a character code, paste it here and you&apos;ll import it
                                </Typography>
                                <TextField
                                    variant={'outlined'}
                                    multiline
                                    fullWidth
                                    inputProps={{ id: 'importField' }}
                                    style={{ marginTop: 16 }}
                                    rows={Math.floor(document.body.scrollHeight / 100)}
                                />
                                <Collapse in={nameIsEqual}>
                                    <Box display={'flex'} flexDirection={'column'} mt={1}>
                                        <Typography variant={'h5'} style={{ color: theme.palette.error.main }}>
                                            The character you are trying to import already exists in your database.
                                            Please choose a new name for it.
                                        </Typography>
                                        <Box mt={1} flex={1}>
                                            <TextField
                                                id={'newNameField'}
                                                value={newName}
                                                variant={'outlined'}
                                                fullWidth
                                                onChange={(e) => setNewName(e.target.value)}
                                            />
                                        </Box>
                                    </Box>
                                </Collapse>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={(e) => {
                                        e.stopPropagation()
                                        const value = document.getElementById('importField')
                                        const char = JSON.parse(
                                            Base64.decode((value as HTMLInputElement).value),
                                        ) as Character
                                        if (char) {
                                            let idSame = false
                                            if (newName !== '') {
                                                if (characters.findIndex((c) => c.id === char.id) !== -1) {
                                                    idSame = true
                                                }
                                                let nameSame = false
                                                if (characters.findIndex((c) => c.name === newName) !== -1) {
                                                    nameSame = true
                                                }
                                                if (!nameSame) {
                                                    importCharacter({
                                                        ...char,
                                                        id: idSame ? uuidv4() : char.id,
                                                        name: newName,
                                                    })
                                                    setNameIsEqual(false)
                                                    setImportCharacterModalVisible(false)
                                                } else {
                                                    setNewName('')
                                                }
                                            } else {
                                                let nameSame = false
                                                if (characters.findIndex((c) => c.id === char.id) !== -1) {
                                                    idSame = true
                                                }
                                                if (characters.findIndex((c) => c.name === char.name) !== -1) {
                                                    nameSame = true
                                                }
                                                if (!nameSame) {
                                                    importCharacter({
                                                        ...char,
                                                        id: idSame ? uuidv4() : char.id,
                                                    })
                                                    setNameIsEqual(false)
                                                    setImportCharacterModalVisible(false)
                                                } else {
                                                    setNameIsEqual(true)
                                                }
                                            }
                                        }
                                    }}
                                    style={{ marginTop: 16 }}
                                >
                                    Import
                                </Button>
                            </>
                        )}
                    </Box>
                </Box>
            </Modal>
        </>
    )
}

export default EditorPage

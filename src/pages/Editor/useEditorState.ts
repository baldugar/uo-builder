import { Dispatch, useCallback, useLayoutEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import GeneralStateActions, { GeneralStateAction } from 'store/GeneralState/GeneralState.actions'
import { AppState } from 'store/store'
import { Character } from 'utils/types'

interface EditorStateReturn {
    characters: Character[]
    selectedCharacterID: string | undefined
    addCharacter: (payload: string) => void
    removeCharacter: (payload: string) => void
    setSelectedCharacter: (payload: string | undefined) => void
    skillUrl: string | undefined
    setSkillURL: (value: string | undefined) => void
    isLoading: boolean
    saveCharacter: (payload: Character) => void
    importCharacter: (payload: Character) => void
    saveModalVisible: boolean
    setSaveModalVisible: (payload: boolean) => void
    setCharacters: (payload: Character[]) => void
}

export const useEditorState: () => EditorStateReturn = () => {
    const dispatch = useDispatch<Dispatch<GeneralStateActions>>()
    const [skillUrl, setSkillURL] = useState<string>()
    const [isLoading, setIsLoading] = useState(true)
    const [saveModalVisible, setSaveModalVisible] = useState(false)
    const { characters, selectedCharacterID } = useSelector((appState: AppState) => ({
        characters: appState.generalState.characters,
        selectedCharacterID: appState.generalState.selectedCharacterID,
    }))
    const addCharacter = useCallback(
        (payload: string) => {
            dispatch({
                type: GeneralStateAction.ADD_CHARACTER,
                payload,
            })
        },
        [dispatch],
    )
    const setSelectedCharacter = useCallback(
        (payload: string | undefined) => {
            dispatch({
                type: GeneralStateAction.SET_SELECTED_CHARACTER,
                payload,
            })
        },
        [dispatch],
    )
    const removeCharacter = useCallback(
        (payload: string) => {
            dispatch({
                type: GeneralStateAction.REMOVE_CHARACTER,
                payload,
            })
            setTimeout(() => {
                if (payload === selectedCharacterID) {
                    setSelectedCharacter(undefined)
                    setSkillURL(undefined)
                }
            }, 250)
        },
        [dispatch, selectedCharacterID],
    )
    const setCharacters = useCallback(
        (payload: Character[]) => {
            dispatch({
                type: GeneralStateAction.SET_CHARACTERS,
                payload,
            })
        },
        [dispatch],
    )
    const saveCharacter = useCallback(
        (payload: Character) => {
            dispatch({
                type: GeneralStateAction.SAVE_CHARACTER,
                payload,
            })
        },
        [dispatch],
    )
    const importCharacter = useCallback(
        (payload: Character) => {
            dispatch({
                type: GeneralStateAction.IMPORT_CHARACTER,
                payload,
            })
        },
        [dispatch],
    )

    useLayoutEffect(() => {
        const charList = localStorage.getItem('uo-builder-charlist')
        if (charList) {
            setCharacters(JSON.parse(charList))
        }
        setIsLoading(false)
    }, [])

    return {
        characters,
        selectedCharacterID,
        skillUrl,
        setSkillURL,
        addCharacter,
        removeCharacter,
        setSelectedCharacter,
        isLoading,
        saveCharacter,
        saveModalVisible,
        setSaveModalVisible,
        setCharacters,
        importCharacter,
    }
}

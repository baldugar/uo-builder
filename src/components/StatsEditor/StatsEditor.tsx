import { Button, Grid, Typography } from '@material-ui/core'
import React from 'react'

export interface StatsProps {
    STR: number
    DEX: number
    INT: number
    handleClick: (event: React.MouseEvent<HTMLButtonElement>) => void
}

const StatsEditor = (props: StatsProps): JSX.Element => {
    const { DEX, INT, STR, handleClick } = props

    return (
        <Grid container direction={'row'}>
            <Grid item xs={12} md={4}>
                <Typography>STR</Typography>
                <Button
                    id={'STR'}
                    onClick={handleClick}
                    style={{
                        backgroundColor: 'rgba(0,0,0,0.12)',
                        boxShadow: '0px 2px 4px 0px rgba(0,0,0,0.75)',
                        maxWidth: 50,
                    }}
                >
                    {STR}
                </Button>
            </Grid>
            <Grid item xs={12} md={4}>
                <Typography>DEX</Typography>
                <Button
                    id={'DEX'}
                    onClick={handleClick}
                    style={{
                        backgroundColor: 'rgba(0,0,0,0.12)',
                        boxShadow: '0px 2px 4px 0px rgba(0,0,0,0.75)',
                        maxWidth: 50,
                    }}
                >
                    {DEX}
                </Button>
            </Grid>
            <Grid item xs={12} md={4}>
                <Typography>INT</Typography>
                <Button
                    id={'INT'}
                    onClick={handleClick}
                    style={{
                        backgroundColor: 'rgba(0,0,0,0.12)',
                        boxShadow: '0px 2px 4px 0px rgba(0,0,0,0.75)',
                        maxWidth: 50,
                    }}
                >
                    {INT}
                </Button>
            </Grid>
        </Grid>
    )
}

export default StatsEditor

import { Checkbox, FormControlLabel, Snackbar } from '@material-ui/core'
import {
    Box,
    ButtonBase,
    ClickAwayListener,
    ListSubheader,
    Grid,
    MenuItem,
    Paper,
    Popper,
    Slider,
    TextField,
    Tooltip,
    Typography,
} from '@material-ui/core'
import SaveIcon from '@material-ui/icons/Save'
import SkillCard from 'components/SkillCard'
import SkillSelector from 'components/SkillSelector'
import StatsEditor from 'components/StatsEditor'
import React, { useEffect, useState } from 'react'
import allSkills from 'utils/skills.json'
import { Character, Skill } from 'utils/types'
import { sortBy } from 'lodash'

export interface CharacterEditorProps {
    character: Character
    saveCharacter: (char: Character) => void
    setSkillURL: (url: string | undefined) => void
    skillUrl: string | undefined
}

const CharacterEditor = (props: CharacterEditorProps): JSX.Element => {
    const { character, saveCharacter, skillUrl, setSkillURL } = props

    const [open, setIsOpen] = useState(false)
    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null)

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        console.log('CLICK', event.currentTarget)
        setAnchorEl(event.currentTarget)
        setIsOpen((prev) => !prev)
    }

    const [STR, setSTR] = useState(character.stats.strength)
    const [DEX, setDEX] = useState(character.stats.dexterity)
    const [INT, setINT] = useState(character.stats.intelligence)
    const [selectedSkill, setSelectedSkill] = useState('0')
    const [skills, setSkills] = useState(character.skillSet.skills)
    const [skillToRemove, setSkillToRemove] = useState<string>()
    const [justSaved, setJustSaved] = useState(false)

    const addSkill = (skID: string) => {
        const skill = allSkills.find((sk) => sk.id === skID)
        if (skill) {
            setSkills([
                ...skills,
                {
                    ...skill,
                    value: 0,
                },
            ])
        }
    }

    const removeSkill = (name: string) => {
        setSkills([...skills.filter((sk) => sk.name !== name)])
    }

    const setSkillValue = (name: string, value: number) => {
        setSkills([
            ...skills.map((sk) => {
                if (sk.name === name) {
                    return {
                        ...sk,
                        value: value,
                    }
                }
                return sk
            }),
        ])
    }

    const getSliderValue = (anchorEl: HTMLButtonElement | null) => {
        if (anchorEl) {
            switch (anchorEl.id) {
                case 'STR':
                    return STR
                case 'DEX':
                    return DEX
                case 'INT':
                    return INT
                default:
                    const skName = anchorEl.id.split('.')[1]
                    const skillValue = skills.find((sk) => sk.name === skName)?.value
                    return skillValue ? skillValue : 0
            }
        }
    }

    const getSliderMax = (anchorEl: HTMLButtonElement | null) => {
        if (anchorEl) {
            switch (anchorEl.id) {
                case 'STR':
                case 'DEX':
                case 'INT':
                    return 100
                default:
                    return 120
            }
        }
    }

    const checkWeaponSpecialSkill = (): number => {
        const weaponSkill = skills.find(
            (sk) =>
                (sk.name === 'Archery' ||
                    sk.name === 'Fencing' ||
                    sk.name === 'Mace Fighting' ||
                    sk.name === 'Swordsmanship' ||
                    sk.name === 'Wrestling') &&
                sk.value > 0,
        )
        if (weaponSkill) {
            const armsLoreSkill = skills.find((sk) => sk.name === 'Arms Lore')
            return (0.1 + (armsLoreSkill ? (0.1 * armsLoreSkill.value) / 100 : 0)) * 100
        }
        return 0
    }

    const checkHamstring = (): number => {
        const firstSkill = skills.find(
            (sk) =>
                (sk.name === 'Anatomy' ||
                    sk.name === 'Arms Lore' ||
                    sk.name === 'Forensic Evaluation' ||
                    sk.name === 'Tracking' ||
                    sk.name === 'Wrestling') &&
                sk.value >= 80,
        )
        if (firstSkill) {
            const secondSkill = skills.find(
                (sk) =>
                    (sk.name === 'Anatomy' ||
                        sk.name === 'Arms Lore' ||
                        sk.name === 'Forensic Evaluation' ||
                        sk.name === 'Tracking' ||
                        sk.name === 'Wrestling') &&
                    sk.value >= 80 &&
                    sk.name !== firstSkill.name,
            )
            if (secondSkill) {
                return 2 + 2 * (4 * (DEX / 100))
            }
        }
        return 0
    }

    const checkParry = (): number => {
        const parrySkill = skills.find((sk) => sk.name === 'Parrying')
        if (parrySkill) {
            return parrySkill.value / 100
        }
        return 0
    }

    useEffect(() => {
        setSTR(character.stats.strength)
        setDEX(character.stats.dexterity)
        setINT(character.stats.intelligence)
        setSkills(character.skillSet.skills)
    }, [character])

    const createDropdownList = (): JSX.Element[] => {
        // const remainingSkills = [
        //     ...sortBy(allSkills, ['name']).filter((sk) => skills.filter((mySk) => mySk.id === sk.id).length === 0),
        // ]
        const categoryNone: Skill[] = [{ category: 'None', id: '0', name: '', url: '', value: 0 }]
        // const categoryCombat: Skill[] = [...remainingSkills.filter((sk) => sk.category === 'Combat')]
        // const categoryMagic: Skill[] = [...remainingSkills.filter((sk) => sk.category === 'Magic')]
        // const categoryBard: Skill[] = [...remainingSkills.filter((sk) => sk.category === 'Bard')]
        // const categoryThieving: Skill[] = [...remainingSkills.filter((sk) => sk.category === 'Thieving')]
        // const categoryTrade: Skill[] = [...remainingSkills.filter((sk) => sk.category === 'Trade')]
        // const categoryWilderness: Skill[] = [...remainingSkills.filter((sk) => sk.category === 'Wilderness')]
        // const categoryMisc: Skill[] = [...remainingSkills.filter((sk) => sk.category === 'Misc')]
        const thingsToReturn: JSX.Element[] = []
        categoryNone.forEach((sk) => {
            thingsToReturn.push(
                <MenuItem style={{ height: 36 }} key={sk.id} value={sk.id}>
                    {sk.name}
                </MenuItem>,
            )
        })
        sortBy(
            allSkills.filter((sk) => skills.filter((mySk) => mySk.id === sk.id).length === 0),
            ['name'],
        ).map((sk) => {
            thingsToReturn.push(
                <MenuItem style={{ height: 36 }} key={sk.id} value={sk.id}>
                    {sk.name}
                </MenuItem>,
            )
        })

        // if (categoryCombat.length > 0) {
        //     categoryCombat.forEach((sk) => {
        //         thingsToReturn.push(
        //             <MenuItem style={{ height: 36 }} key={sk.id} value={sk.id}>
        //                 {sk.name}
        //             </MenuItem>,
        //         )
        //     })
        // }

        // if (categoryMagic.length > 0) {
        //     categoryMagic.forEach((sk) => {
        //         thingsToReturn.push(
        //             <MenuItem style={{ height: 36 }} key={sk.id} value={sk.id}>
        //                 {sk.name}
        //             </MenuItem>,
        //         )
        //     })
        // }

        // if (categoryBard.length > 0) {
        //     categoryBard.forEach((sk) => {
        //         thingsToReturn.push(
        //             <MenuItem style={{ height: 36 }} key={sk.id} value={sk.id}>
        //                 {sk.name}
        //             </MenuItem>,
        //         )
        //     })
        // }

        // if (categoryThieving.length > 0) {
        //     categoryThieving.forEach((sk) => {
        //         thingsToReturn.push(
        //             <MenuItem style={{ height: 36 }} key={sk.id} value={sk.id}>
        //                 {sk.name}
        //             </MenuItem>,
        //         )
        //     })
        // }

        // if (categoryTrade.length > 0) {
        //     categoryTrade.forEach((sk) => {
        //         thingsToReturn.push(
        //             <MenuItem style={{ height: 36 }} key={sk.id} value={sk.id}>
        //                 {sk.name}
        //             </MenuItem>,
        //         )
        //     })
        // }

        // if (categoryWilderness.length > 0) {
        //     categoryWilderness.forEach((sk) => {
        //         thingsToReturn.push(
        //             <MenuItem style={{ height: 36 }} key={sk.id} value={sk.id}>
        //                 {sk.name}
        //             </MenuItem>,
        //         )
        //     })
        // }

        // if (categoryMisc.length > 0) {
        //     categoryMisc.forEach((sk) => {
        //         thingsToReturn.push(
        //             <MenuItem style={{ height: 36 }} key={sk.id} value={sk.id}>
        //                 {sk.name}
        //             </MenuItem>,
        //         )
        //     })
        // }

        return thingsToReturn
    }

    return (
        <>
            <Box
                height={1}
                display={'flex'}
                flexDirection={'column'}
                justifyContent={'flex-start'}
                paddingLeft={1}
                paddingRight={1}
                paddingBottom={2}
                width={{ xs: 1, sm: '25vw' }}
            >
                <Box textAlign={'center'}>
                    <StatsEditor DEX={DEX} INT={INT} STR={STR} handleClick={handleClick} />
                </Box>
                <Box mt={2}>
                    <SkillSelector
                        addSkill={addSkill}
                        createDropdownList={createDropdownList}
                        selectedSkill={selectedSkill}
                        setSelectedSkill={setSelectedSkill}
                        setSkillURL={setSkillURL}
                        skills={skills}
                    />
                </Box>
                <Box mt={2} mb={1}>
                    <Grid container>
                        <Grid
                            container
                            item
                            xs={12}
                            lg={6}
                            direction={'column'}
                            justify={'center'}
                            alignItems={'center'}
                        >
                            <Typography>Stat Points</Typography>
                            <Box display={'flex'} flexDirection={'row'} justifyContent={'center'} alignItems={'center'}>
                                <Typography color={STR + DEX + INT > 225 ? 'error' : 'textPrimary'}>
                                    {STR + DEX + INT}&nbsp;
                                </Typography>
                                <Typography>{` / 225`}</Typography>
                            </Box>
                        </Grid>
                        <Grid
                            container
                            item
                            xs={12}
                            lg={6}
                            direction={'column'}
                            justify={'center'}
                            alignItems={'center'}
                        >
                            <Typography>Skill Points</Typography>
                            <Box display={'flex'} flexDirection={'row'} justifyContent={'center'} alignItems={'center'}>
                                <Typography
                                    color={skills.reduce((a, b) => a + b.value, 0) > 700 ? 'error' : 'textPrimary'}
                                >
                                    {`${skills.reduce((a, b) => a + b.value, 0)}`}&nbsp;
                                </Typography>
                                <Typography>{` / 700`}</Typography>
                            </Box>
                        </Grid>
                    </Grid>
                </Box>
                <Box mt={1} mb={1} flex={1} style={{ overflowY: 'auto' }}>
                    <Grid container>
                        {skills.map((sk, index) => {
                            return (
                                <Grid key={sk.id} item xs={12} lg={6} style={{ padding: 5 }}>
                                    <SkillCard
                                        key={index}
                                        handleClick={handleClick}
                                        index={index}
                                        removeSkill={removeSkill}
                                        setSkillToRemove={setSkillToRemove}
                                        setSkillURL={setSkillURL}
                                        sk={sk}
                                        skillToRemove={skillToRemove}
                                    />
                                </Grid>
                            )
                        })}
                    </Grid>
                </Box>
                <Box marginTop={'auto'}>
                    <ButtonBase
                        onClick={() => {
                            saveCharacter({
                                ...character,
                                skillSet: {
                                    skills: [...skills],
                                },
                                stats: {
                                    dexterity: DEX,
                                    intelligence: INT,
                                    strength: STR,
                                },
                            })
                            setJustSaved(true)
                        }}
                        style={{
                            width: '100%',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: 56,
                            borderRadius: 8,
                            backgroundColor: 'rgba(0,0,0,0.12)',
                            boxShadow: '0px 2px 4px 0px rgba(0,0,0,0.75)',
                        }}
                    >
                        <SaveIcon />
                    </ButtonBase>
                </Box>
            </Box>
            <Box display={'flex'} flexDirection={'column'} flex={1} paddingBottom={2}>
                <Grid container>
                    <Grid item xs={'auto'}>
                        <Tooltip
                            placement={'top'}
                            title={
                                <>
                                    Chance on triggering a special weapon&apos;s attack:
                                    {` ${checkWeaponSpecialSkill().toFixed(2)}%.`}
                                    <br /> You need any weapon skill &gt; 0. Arms lore increases it.
                                </>
                            }
                        >
                            <FormControlLabel
                                disabled
                                control={<Checkbox checked={checkWeaponSpecialSkill() > 0} />}
                                label="Weapon Skill"
                            />
                        </Tooltip>
                    </Grid>
                    <Grid item xs={'auto'}>
                        <Tooltip
                            placement={'top'}
                            title={
                                <>
                                    You can perform a disarm movement.
                                    <br />
                                    You need any weapon skill &gt;= 80 & Arms Lore &gt;= 80
                                </>
                            }
                        >
                            <FormControlLabel
                                disabled
                                control={
                                    <Checkbox
                                        checked={
                                            skills.find((sk) => sk.name === 'Arms Lore' && sk.value >= 80) !==
                                                undefined &&
                                            skills.find(
                                                (sk) =>
                                                    (sk.name === 'Archery' ||
                                                        sk.name === 'Fencing' ||
                                                        sk.name === 'Mace Fighting' ||
                                                        sk.name === 'Swordsmanship' ||
                                                        sk.name === 'Wrestling') &&
                                                    sk.value >= 80,
                                            ) !== undefined
                                        }
                                    />
                                }
                                label="Disarm"
                            />
                        </Tooltip>
                    </Grid>
                    <Grid item xs={'auto'}>
                        <Tooltip
                            placement={'top'}
                            title={
                                <>
                                    Your hamstring will last for ${checkHamstring().toFixed(2)} seconds. <br /> You need
                                    2 among Anatomy, Arms Lore, Forensic Evaluation, Tracking, and Wrestling &gt;= 80
                                </>
                            }
                        >
                            <FormControlLabel
                                disabled
                                control={<Checkbox checked={checkHamstring() > 0} />}
                                label="Hamstring"
                            />
                        </Tooltip>
                    </Grid>
                    <Grid item xs={'auto'}>
                        <Tooltip
                            placement={'top'}
                            title={
                                <>
                                    Parry Attacks: {(50 * checkParry()).toFixed(2)}%
                                    <br />
                                    Parry Spells: {(25 * checkParry()).toFixed(2)}%
                                </>
                            }
                        >
                            <FormControlLabel
                                disabled
                                control={<Checkbox checked={checkParry() > 0} />}
                                label="Parry"
                            />
                        </Tooltip>
                    </Grid>
                </Grid>
                {skillUrl !== undefined && <iframe width={'100%'} height={'100%'} src={skillUrl} />}
            </Box>
            <Popper anchorEl={anchorEl} open={open}>
                <ClickAwayListener
                    onClickAway={() => {
                        setAnchorEl(null)
                        setIsOpen(false)
                    }}
                >
                    <Paper elevation={3} style={{ paddingLeft: 16, paddingRight: 16 }}>
                        <Box
                            width={200}
                            height={80}
                            display={'flex'}
                            flexDirection={'column'}
                            justifyContent={'center'}
                            alignItems={'center'}
                        >
                            <Slider
                                value={getSliderValue(anchorEl)}
                                marks
                                min={0}
                                max={getSliderMax(anchorEl)}
                                onChange={(_, value) => {
                                    if (typeof value === 'number' && anchorEl) {
                                        switch (anchorEl.id) {
                                            case 'STR':
                                                setSTR(value)
                                                break
                                            case 'DEX':
                                                setDEX(value)
                                                break
                                            case 'INT':
                                                setINT(value)
                                                break
                                            default:
                                                const skName = anchorEl.id.split('.')[1]
                                                setSkillValue(skName, value)
                                        }
                                    }
                                }}
                            />
                            <Box>
                                <TextField
                                    value={getSliderValue(anchorEl)}
                                    type={'number'}
                                    InputProps={{
                                        inputProps: {
                                            min: 0,
                                            max: 100,
                                        },
                                    }}
                                    onChange={(e) => {
                                        if (Number(e.target.value) >= 0 && Number(e.target.value) <= 100) {
                                            if (anchorEl) {
                                                switch (anchorEl.id) {
                                                    case 'STR':
                                                        setSTR(Number(e.target.value))
                                                        break
                                                    case 'DEX':
                                                        setDEX(Number(e.target.value))
                                                        break
                                                    case 'INT':
                                                        setINT(Number(e.target.value))
                                                        break
                                                    default:
                                                        const skName = anchorEl.id.split('.')[1]
                                                        setSkillValue(skName, Number(e.target.value))
                                                }
                                            }
                                        }
                                    }}
                                    style={{ width: 45 }}
                                />
                            </Box>
                        </Box>
                    </Paper>
                </ClickAwayListener>
            </Popper>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                open={justSaved}
                autoHideDuration={3000}
                message="Character saved!"
                onClose={() => {
                    setJustSaved(false)
                }}
            />
        </>
    )
}

export default CharacterEditor

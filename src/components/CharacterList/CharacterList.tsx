import { yupResolver } from '@hookform/resolvers/yup'
import { Box, ButtonBase, Collapse, IconButton, TextField, Tooltip, Typography } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import CancelIcon from '@material-ui/icons/Cancel'
import CheckIcon from '@material-ui/icons/Check'
import SaveIcon from '@material-ui/icons/Save'
import CharacterRow from 'components/CharacterRow'
import React, { useMemo, useRef, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward'
import PublishIcon from '@material-ui/icons/Publish'
import { Character } from 'utils/types'
import * as yup from 'yup'

export interface CharacterListProps {
    characters: Character[]
    selectedCharacterID: string | undefined
    addCharacter: (name: string) => void
    removeCharacter: (id: string) => void
    setSelectedCharacter: (id: string | undefined) => void
    setImportDatabaseModal: (visible: boolean) => void
    setImportCharacterModal: (visible: boolean) => void
    setExportCharacterModal: (character: Character) => void
    setSaveModalVisible: (visible: boolean) => void
}

const CharacterList = (props: CharacterListProps): JSX.Element => {
    const {
        characters,
        selectedCharacterID,
        removeCharacter,
        addCharacter,
        setSelectedCharacter,
        setSaveModalVisible,
        setImportDatabaseModal,
        setImportCharacterModal,
        setExportCharacterModal,
    } = props
    const schema = useMemo(
        () =>
            yup.object().shape({
                name: yup
                    .string()
                    .notOneOf(
                        characters.map((c) => c.name),
                        "Names can't be repeated",
                    )
                    .min(1)
                    .max(255)
                    .required('Need to add a name'),
            }),
        [characters],
    )
    const { control, formState, setValue, watch } = useForm<{ name: string }>({
        mode: 'all',
        reValidateMode: 'onChange',
        resolver: yupResolver(schema),
    })
    const watchName = watch('name', '')
    const { isValid } = formState
    const [newChar, setNewChar] = useState(false)
    const newCharFieldRef = useRef<any>()

    const renderCharacter = (character: Character) => {
        console.log(selectedCharacterID)
        return (
            <CharacterRow
                key={character.id}
                character={character}
                selectedCharacterID={selectedCharacterID}
                removeCharacter={removeCharacter}
                setSelectedCharacter={setSelectedCharacter}
                setExportCharacterModal={setExportCharacterModal}
            />
        )
    }
    return (
        <Box height={1} display={'flex'} flexDirection={'column'} justifyContent={'flex-start'} paddingBottom={2}>
            <Box display={'flex'} justifyContent={'center'} alignItems={'center'} flexWrap={'wrap'}>
                <Typography variant={'h4'}>Characters</Typography>
                <Box>
                    <Tooltip placement={'top'} title={'Save database'}>
                        <IconButton
                            onClick={() => {
                                localStorage.setItem('uo-builder-charlist', JSON.stringify(characters))
                                if (characters.length > 0) {
                                    setSaveModalVisible(true)
                                }
                            }}
                            style={{ marginRight: 8 }}
                        >
                            <SaveIcon />
                        </IconButton>
                    </Tooltip>
                    <Tooltip placement={'top'} title={'Import database'}>
                        <IconButton
                            onClick={() => {
                                setImportDatabaseModal(true)
                            }}
                            style={{ marginRight: 8 }}
                        >
                            <PublishIcon />
                        </IconButton>
                    </Tooltip>
                    <Tooltip placement={'top'} title={'Import character'}>
                        <IconButton
                            onClick={() => {
                                setImportCharacterModal(true)
                            }}
                            style={{ marginLeft: 8 }}
                        >
                            <ArrowUpwardIcon />
                        </IconButton>
                    </Tooltip>
                </Box>
            </Box>
            <Box flex={1} marginTop={2} style={{ overflowY: 'auto', padding: 5 }}>
                {characters.map(renderCharacter)}
            </Box>
            <Box mt={2}>
                <Collapse in={!newChar}>
                    <ButtonBase
                        onClick={() => setNewChar(true)}
                        style={{
                            margin: '0 auto',
                            width: 'calc(100% - 14px)',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: 56,
                            borderRadius: 8,
                            backgroundColor: 'rgba(0,0,0,0.12)',
                            boxShadow: '0px 2px 4px 0px rgba(0,0,0,0.75)',
                        }}
                    >
                        <AddIcon />
                    </ButtonBase>
                </Collapse>
                <Collapse
                    in={newChar}
                    style={{ width: '100%' }}
                    onEntered={() => {
                        if (newCharFieldRef.current !== undefined) {
                            newCharFieldRef.current.focus()
                        }
                    }}
                >
                    <Box
                        flex={1}
                        display={'flex'}
                        justifyContent={'space-between'}
                        alignItems={'center'}
                        height={56}
                        paddingX={'16px'}
                    >
                        <Controller
                            control={control}
                            name={'name'}
                            render={(props) => (
                                <TextField
                                    inputRef={newCharFieldRef}
                                    fullWidth
                                    helperText={props.fieldState.error?.message}
                                    onKeyPress={(e) => {
                                        if (e.key === 'Enter' && isValid) {
                                            addCharacter(watchName)
                                            setValue('name', '')
                                            setNewChar(false)
                                        }
                                    }}
                                    {...props.field}
                                />
                            )}
                            defaultValue={''}
                        />
                        <Box display={'flex'} justifyContent={'space-between'}>
                            <IconButton
                                disabled={!isValid}
                                onClick={() => {
                                    addCharacter(watchName)
                                    setValue('name', '')
                                    setNewChar(false)
                                }}
                            >
                                <CheckIcon />
                            </IconButton>
                            <IconButton
                                onClick={() => {
                                    setNewChar(false)
                                    setValue('name', '')
                                }}
                            >
                                <CancelIcon />
                            </IconButton>
                        </Box>
                    </Box>
                </Collapse>
            </Box>
        </Box>
    )
}

export default CharacterList

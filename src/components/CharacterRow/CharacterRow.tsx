import { Box, ButtonBase, Collapse, Grid, IconButton, Typography } from '@material-ui/core'
import CancelIcon from '@material-ui/icons/Cancel'
import CheckIcon from '@material-ui/icons/Check'
import DeleteIcon from '@material-ui/icons/Delete'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward'
import React, { useState } from 'react'
import { Character } from 'utils/types'

export interface CharacterRowProps {
    character: Character
    selectedCharacterID: string | undefined
    removeCharacter: (id: string) => void
    setSelectedCharacter: (id: string | undefined) => void
    setExportCharacterModal: (character: Character) => void
}

const CharacterRow = (props: CharacterRowProps): JSX.Element => {
    const { character, selectedCharacterID, removeCharacter, setSelectedCharacter, setExportCharacterModal } = props
    const [confirmDelete, setConfirmDelete] = useState(false)

    console.log(selectedCharacterID, character.id)

    return (
        <Box
            width={1}
            style={{
                backgroundColor:
                    selectedCharacterID !== undefined && selectedCharacterID === character.id ? '#3f51b5' : 'white',
                boxShadow: '0px 2px 4px 0px rgba(0,0,0,0.75)',
                borderRadius: 8,
                marginBottom: 5,
            }}
        >
            <ButtonBase
                onClick={() => setSelectedCharacter(character.id)}
                style={{ width: '100%', paddingTop: 4, paddingBottom: 4, borderRadius: 8 }}
            >
                <Grid container>
                    <Grid container item xs={8} justify={'flex-start'} alignItems={'center'}>
                        <Box flex={1} display={'flex'} justifyContent={'flex-start'} paddingX={2}>
                            <Typography
                                style={{
                                    width: '100%',
                                    wordBreak: 'break-word',
                                    textAlign: 'left',
                                    color:
                                        selectedCharacterID !== undefined && selectedCharacterID === character.id
                                            ? 'white'
                                            : 'black',
                                }}
                            >
                                {character.name}
                            </Typography>
                        </Box>
                    </Grid>
                    <Grid container item xs={4} justify={'flex-end'} alignItems={'center'}>
                        <Collapse in={!confirmDelete} style={{ width: '100%' }}>
                            <Box flex={1} display={'flex'} flexDirection={'row'} justifyContent={'flex-end'} pr={0.5}>
                                <IconButton
                                    onClick={(e) => {
                                        e.preventDefault()
                                        e.stopPropagation()
                                        setExportCharacterModal(character)
                                    }}
                                >
                                    <ArrowDownwardIcon
                                        htmlColor={
                                            selectedCharacterID !== undefined && selectedCharacterID === character.id
                                                ? 'rgba(255,255,255,0.54)'
                                                : 'rgba(0,0,0,0.54)'
                                        }
                                    />
                                </IconButton>
                                <IconButton
                                    onClick={(e) => {
                                        e.preventDefault()
                                        e.stopPropagation()
                                        setConfirmDelete(true)
                                    }}
                                >
                                    <DeleteIcon
                                        htmlColor={
                                            selectedCharacterID !== undefined && selectedCharacterID === character.id
                                                ? 'rgba(255,255,255,0.54)'
                                                : 'rgba(0,0,0,0.54)'
                                        }
                                    />
                                </IconButton>
                            </Box>
                        </Collapse>
                        <Collapse in={confirmDelete} style={{ width: '100%' }}>
                            <Box flex={1} display={'flex'} flexDirection={'row'} justifyContent={'flex-end'} pr={0.5}>
                                <IconButton
                                    onClick={(e) => {
                                        e.preventDefault()
                                        e.stopPropagation()
                                        removeCharacter(character.id)
                                    }}
                                >
                                    <CheckIcon
                                        htmlColor={
                                            selectedCharacterID !== undefined && selectedCharacterID === character.id
                                                ? 'rgba(255,255,255,0.54)'
                                                : 'rgba(0,0,0,0.54)'
                                        }
                                    />
                                </IconButton>
                                <IconButton
                                    onClick={(e) => {
                                        e.preventDefault()
                                        e.stopPropagation()
                                        setConfirmDelete(false)
                                    }}
                                >
                                    <CancelIcon
                                        htmlColor={
                                            selectedCharacterID !== undefined && selectedCharacterID === character.id
                                                ? 'rgba(255,255,255,0.54)'
                                                : 'rgba(0,0,0,0.54)'
                                        }
                                    />
                                </IconButton>
                            </Box>
                        </Collapse>
                    </Grid>
                </Grid>
            </ButtonBase>
        </Box>
    )

    // return (
    //     <TableRow
    //         onClick={() => setSelectedCharacter(character.id)}
    //         selected={selectedCharacterID !== undefined && selectedCharacterID === character.id}
    //         style={{ width: '100%' }}
    //     >
    //         <TableCell style={{ flexGrow: 1 }}>{character.name}</TableCell>
    //         <TableCell align={'right'}>
    //             <Collapse in={!confirmDelete}>
    //                 <IconButton
    //                     onClick={(e) => {
    //                         e.preventDefault()
    //                         e.stopPropagation()
    //                         setConfirmDelete(true)
    //                     }}
    //                 >
    //                     <DeleteIcon />
    //                 </IconButton>
    //             </Collapse>
    //             <Collapse in={confirmDelete}>
    //                 <Box display={'flex'} flexDirection={'row'} justifyContent={'flex-end'}>
    //                     <IconButton
    //                         onClick={(e) => {
    //                             e.preventDefault()
    //                             e.stopPropagation()
    //                             removeCharacter(character.id)
    //                         }}
    //                     >
    //                         <CheckIcon />
    //                     </IconButton>
    //                     <IconButton
    //                         onClick={(e) => {
    //                             e.preventDefault()
    //                             e.stopPropagation()
    //                             setConfirmDelete(false)
    //                         }}
    //                     >
    //                         <CancelIcon />
    //                     </IconButton>
    //                 </Box>
    //             </Collapse>
    //         </TableCell>
    //     </TableRow>
    // )
}

export default CharacterRow

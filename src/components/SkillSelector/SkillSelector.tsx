import { Grid, IconButton, Select, useTheme } from '@material-ui/core'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import InfoIcon from '@material-ui/icons/Info'
import React from 'react'
import allSkills from 'utils/skills.json'
import { Skill } from 'utils/types'

export interface SkillSelectorProps {
    selectedSkill: string
    setSelectedSkill: (value: React.SetStateAction<string>) => void
    createDropdownList: () => JSX.Element[]
    skills: Skill[]
    addSkill: (skID: string) => void
    setSkillURL: (url: string | undefined) => void
}

const SkillSelector = (props: SkillSelectorProps): JSX.Element => {
    const { createDropdownList, selectedSkill, setSelectedSkill, skills, addSkill, setSkillURL } = props
    const theme = useTheme()
    const xsBreakpoint = useMediaQuery(theme.breakpoints.down('lg'))

    return (
        <Grid container direction={'row'}>
            <Grid container item xs={12} md={8} justify={'center'} alignItems={'center'}>
                <Select
                    value={selectedSkill}
                    onChange={(e) => setSelectedSkill(String(e.target.value))}
                    fullWidth
                    style={{ maxWidth: xsBreakpoint ? 128 : 200 }}
                >
                    {createDropdownList().map((el) => el)}
                </Select>
            </Grid>
            <Grid container item xs={12} md={4} direction={'row'} style={{ marginTop: xsBreakpoint ? 12 : 0 }}>
                <Grid container item xs={6} justify={'center'}>
                    <IconButton
                        size={'small'}
                        onClick={() => {
                            if (skills.find((sk) => sk.id === selectedSkill) === undefined) {
                                addSkill(selectedSkill)
                                setSelectedSkill('0')
                            }
                        }}
                        color={'primary'}
                    >
                        <AddCircleIcon />
                    </IconButton>
                </Grid>
                <Grid container item xs={6} justify={'center'}>
                    <IconButton
                        size={'small'}
                        onClick={() => {
                            const infoSelectedSkill = allSkills.find((sk) => sk.id === selectedSkill)
                            setSkillURL(infoSelectedSkill?.url)
                        }}
                    >
                        <InfoIcon />
                    </IconButton>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default SkillSelector

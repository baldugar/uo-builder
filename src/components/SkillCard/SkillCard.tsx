import { Box, Button, Collapse, IconButton, Typography } from '@material-ui/core'
import CancelIcon from '@material-ui/icons/Cancel'
import CheckIcon from '@material-ui/icons/CheckCircle'
import InfoIcon from '@material-ui/icons/Info'
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle'
import React from 'react'
import { Skill } from 'utils/types'

export interface SkillCardProps {
    sk: Skill
    index: number
    handleClick: (event: React.MouseEvent<HTMLButtonElement>) => void
    skillToRemove: string | undefined
    setSkillToRemove: (value: React.SetStateAction<string | undefined>) => void
    setSkillURL: (url: string | undefined) => void
    removeSkill: (name: string) => void
}

const SkillCard = (props: SkillCardProps): JSX.Element => {
    const { handleClick, index, removeSkill, setSkillToRemove, setSkillURL, sk, skillToRemove } = props

    return (
        <Box
            key={sk.id}
            height={76}
            display={'flex'}
            flexDirection={'row'}
            alignItems={'center'}
            p={1}
            borderRadius={8}
            style={{
                boxShadow: '0px 2px 4px 0px rgba(0,0,0,0.75)',
            }}
        >
            <Box display={'flex'} flexDirection={'column'} justifyContent={'center'} alignItems={'flex-start'} flex={1}>
                <Typography>{sk.name}</Typography>
                <Box width={1}>
                    <Box display={'flex'} flexDirection={'row'} justifyContent={'space-between'} width={1}>
                        <Box>
                            <Button
                                id={`sk.${sk.name}`}
                                onClick={handleClick}
                                style={{
                                    backgroundColor: 'rgba(0,0,0,0.12)',
                                    boxShadow: '0px 2px 4px 0px rgba(0,0,0,0.75)',
                                }}
                            >
                                {sk.value}
                            </Button>
                        </Box>
                        <Box display={'flex'} width={1} flexDirection={'column'}>
                            <Collapse
                                in={skillToRemove === undefined || skillToRemove !== sk.name}
                                style={{ width: '100%' }}
                            >
                                <Box display={'flex'} flexDirection={'row'} justifyContent={'space-evenly'} flex={1}>
                                    <IconButton
                                        size={'small'}
                                        onClick={() => {
                                            setSkillToRemove(sk.name)
                                        }}
                                        color={'primary'}
                                    >
                                        <RemoveCircleIcon />
                                    </IconButton>
                                    <IconButton
                                        size={'small'}
                                        onClick={() => {
                                            setSkillURL(sk.url)
                                        }}
                                    >
                                        <InfoIcon />
                                    </IconButton>
                                </Box>
                            </Collapse>
                            <Collapse
                                in={skillToRemove !== undefined && skillToRemove === sk.name}
                                style={{ width: '100%' }}
                            >
                                <Box display={'flex'} flexDirection={'row'} justifyContent={'space-evenly'} flex={1}>
                                    <IconButton
                                        size={'small'}
                                        onClick={(e) => {
                                            e.preventDefault()
                                            e.stopPropagation()
                                            removeSkill(sk.name)
                                        }}
                                    >
                                        <CheckIcon color={'primary'} />
                                    </IconButton>
                                    <IconButton
                                        size={'small'}
                                        onClick={(e) => {
                                            e.preventDefault()
                                            e.stopPropagation()
                                            setSkillToRemove(undefined)
                                        }}
                                    >
                                        <CancelIcon color={'secondary'} />
                                    </IconButton>
                                </Box>
                            </Collapse>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}

export default SkillCard

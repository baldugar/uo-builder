export interface Stats {
    strength: number
    dexterity: number
    intelligence: number
}

export interface Skill {
    id: string
    name: string
    value: number
    url: string
    category: string
}

export interface SkillSet {
    skills: Skill[]
}

export interface Character {
    id: string
    name: string
    stats: Stats
    skillSet: SkillSet
}

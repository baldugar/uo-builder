import { createMuiTheme, responsiveFontSizes, Theme, unstable_createMuiStrictModeTheme } from '@material-ui/core'

// const breakpoints = createBreakpoints({})
// export const smallMobile = 385
// export const smallestMobile = 360

const createTheme = process.env.NODE_ENV === 'production' ? createMuiTheme : unstable_createMuiStrictModeTheme

const theme: Theme = createTheme({
    overrides: {
        MuiContainer: {
            root: {
                height: '100vh',
            },
        },
    },
    // palette: {
    //     primary: {
    //         main: colors.exampleColor,
    //     },
    // },
    // typography: {
    //     h2: { ...fontStyles.ExampleFont },
    // },
})

export const ModifiedTheme = responsiveFontSizes(theme)
